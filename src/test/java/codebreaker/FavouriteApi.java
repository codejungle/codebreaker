package codebreaker;

import org.testng.annotations.Test;

import com.alti.common.ConfigReader;
import com.alti.common.RestUtilities;
import com.alti.common.Specifications;
import com.alti.constants.EndPoints;
import com.alti.reporter.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import junit.framework.Assert;

import static io.restassured.RestAssured.given;

public class FavouriteApi extends Specifications {

	@Test(dataProvider = "commonData", enabled = false)
	public void doFavoriteTest(String hmacid, String devicetype, String source) throws Exception {
		ExtentTestManager.getTest().log(LogStatus.INFO, "Starting Test doFavouriteTest");
		System.out.println(hmacid);
		System.out.println(devicetype);

		Response resp = given().spec(reqSpec).body(
				"{\"operationName\":\"OnboardingPushFavorite\",\"variables\":{\"input\":{\"Entity_Id\":\"K8vZ9171G-7\",\"Device_Type\":\""
						+ devicetype
						+ "\",\"Device_Id\":null,\"Entity_Id_Source\":\"DISCOVERY\",\"Entity_Id_Type\":\"ATTRACTION\",\"Type\":\"FAV\",\"Source\":\""
						+ source
						+ "\",\"Customer_Id\":\"CtRvhBlTWmElAotMuPG51g\",\"Hmac_Id\":\"9d857d7b003cde1d85ff67710d0423c83c625696d0fc8ef5aad0ece744ef8149\",\"Legacy_Id\":\"735394\"}},\"extensions\":{\"persistedQuery\":{\"version\":1,\"sha256Hash\":\"bba28e2776cfe34932008b68547b5390257a0f1cc2332ebabdfe0867c89979e6\"}}}")
				.when().post(EndPoints.GraphQL).then().log().all().spec(resSpec).extract().response();
		ExtentTestManager.getTest().log(LogStatus.INFO, "Response Body : " + resp.asString());
		JsonPath jsonpath = RestUtilities.getJsonPath(resp);
		System.out.println(jsonpath.getString("data.trackingFavoritePush.Source"));
		System.out.println(jsonpath.getString("data.trackingFavoritePush.Date"));
		System.out.println(jsonpath.getString("data.trackingFavoritePush.Entity_Id"));
		System.out.println(jsonpath.getString("data.trackingFavoritePush.Entity_Id_Source"));

		/*
		 * String colname = name; String colCity = city; String loggeduser = user;
		 * 
		 * JSONObject testData = new JSONObject(); testData.put("collegeName", colname);
		 * testData.put("city", colCity); testData.put("loggedInUser", user);
		 * testData.put("status", "Active");
		 * 
		 * ExtentTestManager.getTest().log(LogStatus.INFO, "Request Body : " +
		 * testData); Response resp =
		 * given().relaxedHTTPSValidation().header("Authorization", "Bearer " +
		 * accessToken) .header("Realm",
		 * "altimetrik").spec(reqSpec).body(testData).when().post(EndPoints.
		 * POST_ADDNEWCOLLEGE) .then().log().all().spec(resSpec).extract().response();
		 * ExtentTestManager.getTest().log(LogStatus.INFO, "Response Body : " +
		 * resp.asString()); JsonPath jsonpath = RestUtilities.getJsonPath(resp);
		 * Assert.assertEquals(resp.statusCode(),
		 * Integer.parseInt(ConstantTexts.SUCCESS_STATUS_CODE));
		 * Assert.assertEquals(jsonpath.getString("successMessage"),
		 * "Saved Succesfully"); Assert.assertTrue(verifyCollege(name));
		 */
	}

	@Test(enabled = false)
	public void getFavoriteTest() throws Exception {
		ExtentTestManager.getTest().log(LogStatus.INFO, "Starting Test getFavouriteTest");

		Response resp = given().spec(reqSpec).queryParam("operationName", "OnboardingAttractionAndVenueEvents")
				.queryParam("variables",
						"{\"discoveryAttractionsId\":\"K8vZ917KjPf,K8vZ9174Za7,K8vZ9171G-7,K8vZ9171Jo7\",\"discoveryVenuesId\":\"\",\"legacyAttractionsId\":\"\",\"legacyVenuesId\":\"\",\"loadDiscoveryAttractions\":true,\"loadDiscoveryVenues\":false,\"loadLegacyAttractions\":false,\"loadLegacyVenues\":false,\"geoHash\":null,\"sort\":\"date,asc\",\"locale\":\"en-us\",\"startDateTime\":\"2019-10-24T09:38:17+05:30\"}")
				.queryParam("extensions",
						"{\"persistedQuery\":{\"version\":1,\"sha256Hash\":\"438f449bead84b89a06fb82c1c5e91b75b7d64a5153f8382497bec62b82bd182\"}}")
				.when().get(EndPoints.GraphQL).then().log().all().spec(resSpec).extract().response();
		Assert.assertEquals(resp.getStatusCode(), 200);
		ExtentTestManager.getTest().log(LogStatus.INFO,"Status Code verified");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Response Body : " + resp.asString());
	}
	
	@Test(dataProvider = "commonData",enabled = false)
	public void removeFavoriteTest(String favid) throws Exception {
		ExtentTestManager.getTest().log(LogStatus.INFO, "Starting Test removeFavouriteTest");

		Response resp = given().spec(reqSpec).queryParam("operationName", "OnboardingAttractionAndVenueEvents")
				.queryParam("variables",
						"{\"discoveryAttractionsId\":\"K8vZ917KjPf,K8vZ9174Za7,K8vZ9171G-7,K8vZ9171Jo7\",\"discoveryVenuesId\":\"\",\"legacyAttractionsId\":\"\",\"legacyVenuesId\":\"\",\"loadDiscoveryAttractions\":true,\"loadDiscoveryVenues\":false,\"loadLegacyAttractions\":false,\"loadLegacyVenues\":false,\"geoHash\":null,\"sort\":\"date,asc\",\"locale\":\"en-us\",\"startDateTime\":\"2019-10-24T09:38:17+05:30\"}")
				.queryParam("extensions",
						"{\"persistedQuery\":{\"version\":1,\"sha256Hash\":\"438f449bead84b89a06fb82c1c5e91b75b7d64a5153f8382497bec62b82bd182\"}}")
				.when().get(EndPoints.GraphQL).then().log().all().spec(resSpec).extract().response();
	}
	
	@Test(dataProvider = "commonData", enabled = true)
	public void pushBatchVerification(String hmacid, String devicetype, String source,String entityid) throws Exception {
		ExtentTestManager.getTest().log(LogStatus.INFO, "Starting Test doFavouriteTest");
		System.out.println(hmacid);
		System.out.println(devicetype);

		Response resp = given().spec(reqSpec).body(
				"{\"operationName\":\"OnboardingPushFavorite\",\"variables\":{\"input\":{\"Entity_Id\":\""+entityid+"\",\"Device_Type\":\""
						+ devicetype
						+ "\",\"Device_Id\":null,\"Entity_Id_Source\":\"DISCOVERY\",\"Entity_Id_Type\":\"ATTRACTION\",\"Type\":\"FAV\",\"Source\":\""
						+ source
						+ "\",\"Customer_Id\":\"CtRvhBlTWmElAotMuPG51g\",\"Hmac_Id\":\"9d857d7b003cde1d85ff67710d0423c83c625696d0fc8ef5aad0ece744ef8149\",\"Legacy_Id\":\"735394\"}},\"extensions\":{\"persistedQuery\":{\"version\":1,\"sha256Hash\":\"bba28e2776cfe34932008b68547b5390257a0f1cc2332ebabdfe0867c89979e6\"}}}")
				.when().post(EndPoints.GraphQL).then().log().all().spec(resSpec).extract().response();
		ExtentTestManager.getTest().log(LogStatus.INFO, "Response Body : " + resp.asString());
		JsonPath jsonpath = RestUtilities.getJsonPath(resp);
		System.out.println(jsonpath.getString("data.trackingFavoritePush.Source"));
		System.out.println(jsonpath.getString("data.trackingFavoritePush.Date"));
		System.out.println(jsonpath.getString("data.trackingFavoritePush.Entity_Id"));
		System.out.println(jsonpath.getString("data.trackingFavoritePush.Entity_Id_Source"));
	}


}
