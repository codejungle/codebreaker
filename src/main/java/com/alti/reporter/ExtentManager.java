package com.alti.reporter;

import org.joda.time.DateTime;

import com.relevantcodes.extentreports.ExtentReports;

public class ExtentManager {
	static ExtentReports extent;
	final static String filePath="./test-report/html/Extent.html";
	//final static String filePath="./test-report/html/Extent"+DateTime.now().toString("yyyy-dd-M--HH-mm-ss")+".html";
	
	public synchronized static ExtentReports getReporter() {
		if(extent==null) {
			extent= new ExtentReports(filePath,true);
		}
		return extent;
	}
	
}
