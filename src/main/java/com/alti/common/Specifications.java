package com.alti.common;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import static io.restassured.RestAssured.given;
import com.alti.constants.Paths;
import com.alti.dataprovider.ExcelDataProvider;
import com.alti.reporter.ExtentManager;
import com.alti.reporter.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class Specifications {
	
	Logger log= Logger.getLogger(Specifications.class);
    public RequestSpecification reqSpec;
    public  ResponseSpecification resSpec;
    static Map<String, Object> testcaseMap;
    protected static String accessToken = null;

    @Parameters({"path"})
    @BeforeClass
    public void compleObject (String path,final ITestContext testContext) {

        reqSpec = RestUtilities.getRequestSpecification();
        if(path.equals("NEXT")) {
        	reqSpec.basePath(Paths.NEXT);
        }
		/*
		 * if (path.equals("EMPLOYEE")) { reqSpec.basePath(Paths.EMPLOYEE); } else if
		 * (path.equals("ORCHESTRATE")) { reqSpec.basePath(Paths.ORCHESTRATE); } else if
		 * (path.equals("EE")) { reqSpec.basePath(Paths.EE); } else if
		 * (path.equals("HACKERRANK")) { reqSpec.basePath(Paths.HACKERRANK); }else if
		 * (path.equals("TA")) { reqSpec.basePath(Paths.TA); } else
		 * if(path.equals("API")){ reqSpec.basePath(Paths.API); } else
		 * if(path.equals("WORKSPACE")) reqSpec.basePath(Paths.WORKSPACE);
		 */
    
        resSpec = RestUtilities.getResponseSpecification();
        
        /**
         * Database Initiation
         */
		/*
		 * DatabaseManager databaseManager = new DatabaseManager(); try {
		 * databaseManager.openConnection(); log.info("Database Initialized"); }
		 * catch(Exception e) { e.printStackTrace(); //log.
		 * info("Database is disabled for the tests ,to enable it back change property to 'yes' [database.enabled=no] in 'Executionconfig.properties' file"
		 * ); }
		 */

    }
    
    @BeforeMethod
    public void beforeMethod(Method method) {
    	ExtentTestManager.startTest(method.getName());
    	accessToken = getAccessToken();
    	System.out.println("Before method executed.");
    	//System.out.println(accessToken);
    }
    
    @AfterMethod
    protected void afterMethod(ITestResult result) {
    	if(result.getStatus()== ITestResult.FAILURE) {
    		ExtentTestManager.getTest().log(LogStatus.FAIL, result.getThrowable());
    	}else if(result.getStatus() == ITestResult.SKIP) {
    		ExtentTestManager.getTest().log(LogStatus.SKIP, "Test Skipped", result.getThrowable());
    	} else {
    		ExtentTestManager.getTest().log(LogStatus.PASS, "Test Passed");
    	}
    	ExtentManager.getReporter().endTest(ExtentTestManager.getTest());
    	ExtentManager.getReporter().flush();
    }
    
    protected String getStackTrace(Throwable t) {
    	StringWriter sw= new StringWriter();
    	PrintWriter pw= new PrintWriter(sw);
    	t.printStackTrace(pw);
    	return sw.toString();
    	
    }
    
    @BeforeSuite
	public void setup() throws IOException{
		
		String testDataFileFormat=("excel");
		testdataProvider(testDataFileFormat);
		
		
	}
    
    /**
     * Method to retrieve SSL access token which can be used to authenticate user to Gateway (Keyclock).
     * @return String accessToken
     * 
     */
    public String getAccessToken() {
    	String url="https://pgiam.altimetrik.com/auth/realms/altimetrik/protocol/openid-connect/token";
		
		Response response =given().relaxedHTTPSValidation().
				header("Content-Type", "application/x-www-form-urlencoded").
				formParam("grant_type", "password").
				formParam("client_id", "pg-rdep-public").
				formParam("username", "pg-mgr1@altimetrik.com").
				formParam("password", "Altimetrik@10").
				request().post(url).then().extract().response();
		JsonPath jsonPath = RestUtilities.getJsonPath(response);
		return jsonPath.getString("access_token ");
    }

	public void testdataProvider(String testDataFileFormat) throws IOException{		
		
			//ExcelDataProvider	excelDataProvider=new ExcelDataProvider(prop.getProperty("testDataExcelName"));
			//ExcelDataProvider excelDataProvider= new ExcelDataProvider(ConfigReader.getProperty("testDataExcelName"));
			//System.out.println(excelDataProvider);
			//ExcelDataProvider	excelDataProvider=new ExcelDataProvider("C:\\Users\\rchandra\\Desktop\\PlaygroundTestData.xlsx");
			ExcelDataProvider excelDataProvider = new ExcelDataProvider("./src/test/resources/TestData/"+ConfigReader.getProperty("testDataExcelName"));
			testcaseMap=excelDataProvider.samplePOC();	
				
		
	}
	
	@DataProvider(name="commonData")
	public  static Object[][]  getTestData(Method method) throws Exception {
		Object[][] data = null;
		
		for (String key : testcaseMap.keySet()) {
			
			if (method.getName().equalsIgnoreCase(key)) {
				data	 = (Object[][]) testcaseMap.get(key);
				break;
			}

		}

		
		//System.out.println(data);
		
		
		return data;

	}
	
	/*
	 * public static Object[][] executeQueries(String query) throws
	 * ClassNotFoundException,Exception{ Connection connection =
	 * DatabaseManager.getConnection(); if(connection == null) return (Object[][])
	 * null; ResultSetMetaData rsMetaData = null; ResultSet result=null; Object[][]
	 * finalResult = (Object[][]) null; try { Statement statement =
	 * (Statement)connection.createStatement();
	 * System.out.println("My Query:"+query); result =
	 * statement.executeQuery(query); Thread.sleep(20000); rsMetaData =
	 * (ResultSetMetaData)result.getMetaData(); int columnCount=
	 * rsMetaData.getColumnCount(); ArrayList<Object[]> data = new ArrayList();
	 * Object[] header = new Object[columnCount]; for(int i=1;i<columnCount;i++) {
	 * Object label= rsMetaData.getColumnLabel(i); header[(i - 1)] = label; }
	 * while(result.next()) { Object[] str = new Object[columnCount]; for (int i =
	 * 1; i < columnCount; i++) { Object obj = result.getObject(i); str[(i - 1)] =
	 * obj; } data.add(str); } int resultLength = data.size(); finalResult = new
	 * Object[resultLength][columnCount]; for(int i=0;i<resultLength;i++){ Object[]
	 * row = (Object[]) data.get(i); finalResult[i] = row; }
	 * 
	 * }catch(SQLException e){ e.printStackTrace(); } result.close(); return
	 * finalResult; }
	 */

}
