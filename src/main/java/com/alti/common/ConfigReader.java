package com.alti.common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.testng.Assert;

public class ConfigReader {
	
	public static Logger log= Logger.getLogger(ConfigReader.class);

	static String filePath = "./src/test/resources/Configurations/config.properties";
	static Properties prop;
	static FileInputStream fis;

	public static String getProperty(String key) {
		String value = prop.getProperty(key);
		if (value != null) {
			log.info("value for the property " + key + "= " + value);
		} else {
			Assert.assertTrue(false, "Value not present");
		}
		return value;
	}

	public static void setProperty(String key, String value) throws IOException {
		FileOutputStream out = new FileOutputStream(filePath);
		prop.setProperty(key, value);
		prop.store(out, null);
		out.close();
		
	}

	static {
		try {
			fis = new FileInputStream(filePath);
			prop = new Properties();
			prop.load(fis);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}

	}

}
