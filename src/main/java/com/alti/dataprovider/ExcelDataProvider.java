package com.alti.dataprovider;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelDataProvider {
	
	static String filepath;
	public static InputStream fileToRead;
	public static XSSFWorkbook workbook;
	private XSSFSheet sheet = null;
	private XSSFRow row = null;
	private XSSFCell cell = null;
	static Map<String, Object> testcaseMap = new HashMap<String, Object>();

	public ExcelDataProvider(String filepath) throws IOException {

		fileToRead = new FileInputStream(filepath);
		workbook = new XSSFWorkbook(fileToRead);

	}
	
	public ExcelDataProvider() throws IOException {
	}

	public void ExcelDataProvider1() throws IOException {

		fileToRead = new FileInputStream(filepath);
		workbook = new XSSFWorkbook(fileToRead);

	}

	public int getSheetCount(XSSFWorkbook workbook) {
		int sheetCount = workbook.getNumberOfSheets();
		return sheetCount;
	}

	public String getSheetName(int sheetCount) {
		return workbook.getSheetName(sheetCount);
	}

	public boolean isRowNull(int sheetNumber, int rowNumber) {

		XSSFSheet customSheet = workbook.getSheetAt(sheetNumber);
		MissingCellPolicy rowNull = row.RETURN_BLANK_AS_NULL;
		if (rowNull == null) {
			return true;
		} else if (findNullRowUsingCol(sheetNumber, rowNumber) == true) {
			return true;
		} else {
			return false;
		}

	}

	public boolean findNullRowUsingCol(int sheetNumber, int rowNumber) {

		XSSFSheet customSheet = workbook.getSheetAt(sheetNumber);

		boolean cond = false;
		for (int i = 0; i < getColumnCount(customSheet.getSheetName()); i++) {
			if (getCellData(customSheet.getSheetName(), i, rowNumber).isEmpty()
					|| (getCellData(customSheet.getSheetName(), i, rowNumber) == null)) {
				cond = true;
			} else {
				cond = false;
				break;
			}

		}

		return cond;

	}

	public int getRowCount(String sheetName) {
		int index = workbook.getSheetIndex(sheetName);
		if (index == -1)
			return 0;
		else {
			sheet = workbook.getSheetAt(index);
			int number = sheet.getLastRowNum() + 1;
			return number;
		}
	}

	public int getColumnCount(String sheetName) {
		// check if sheet exists
		if (!isSheetExist(sheetName))
			return -1;

		sheet = workbook.getSheet(sheetName);
		row = sheet.getRow(0);

		if (row == null)
			return -1;

		return row.getLastCellNum();

	}

	public boolean isSheetExist(String sheetName) {
		int index = workbook.getSheetIndex(sheetName);
		if (index == -1) {
			index = workbook.getSheetIndex(sheetName.toUpperCase());
			if (index == -1)
				return false;
			else
				return true;
		} else
			return true;
	}

	public String getCellData(String sheetName, String colName, int rowNum) {
		try {
			if (rowNum <= 0)
				return "";

			int index = workbook.getSheetIndex(sheetName);
			int col_Num = -1;
			if (index == -1)
				return "";

			sheet = workbook.getSheetAt(index);
			row = sheet.getRow(0);
			for (int i = 0; i < row.getLastCellNum(); i++) {

				//// System.out.println(row.getCell(i).getStringCellValue().trim());

				if (row.getCell(i).getStringCellValue().trim().equals(colName.trim()))
					col_Num = i;
			}
			if (col_Num == -1)
				return "";

			sheet = workbook.getSheetAt(index);
			row = sheet.getRow(rowNum - 1);
			if (row == null)
				return "";
			cell = row.getCell(col_Num);

			if (cell == null)
				return "";

			//// System.out.println(cell.getCellType());

			if (cell.getCellType() == Cell.CELL_TYPE_STRING)
				return cell.getStringCellValue();
			else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC || cell.getCellType() == Cell.CELL_TYPE_FORMULA) {

				String cellText = String.valueOf(cell.getNumericCellValue());
				if (HSSFDateUtil.isCellDateFormatted(cell)) {

					double d = cell.getNumericCellValue();

					Calendar cal = Calendar.getInstance();
					cal.setTime(HSSFDateUtil.getJavaDate(d));
					cellText = (String.valueOf(cal.get(Calendar.YEAR))).substring(2);
					cellText = cal.get(Calendar.DAY_OF_MONTH) + "/" + cal.get(Calendar.MONTH) + 1 + "/" + cellText;

					//// System.out.println(cellText);

				}

				return cellText;
			} else if (cell.getCellType() == Cell.CELL_TYPE_BLANK)
				return "";
			else
				return String.valueOf(cell.getBooleanCellValue());

		} catch (Exception e) {

			e.printStackTrace();
			return "row " + rowNum + " or column " + colName + " does not exist in xls";
		}
	}

	public String getCellData(String sheetName, int colNum, int rowNum) {
		try {
			if (rowNum <= 0)
				return "";

			int index = workbook.getSheetIndex(sheetName);

			if (index == -1)
				return "";

			sheet = workbook.getSheetAt(index);
			row = sheet.getRow(rowNum - 1);
			if (row == null)
				return "";
			cell = row.getCell(colNum);
			if (cell == null)
				return "";

			if (cell.getCellType() == Cell.CELL_TYPE_STRING)
				return cell.getStringCellValue();

			else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
				String cellText = String.valueOf(cell.getNumericCellValue()).replace(".0", "");
				return cellText;
			} else if (cell.getCellType() == Cell.CELL_TYPE_FORMULA) {

				String cellText = String.valueOf(cell.getNumericCellValue());
				if (HSSFDateUtil.isCellDateFormatted(cell)) {
					// format in form of M/D/YY
					double d = cell.getNumericCellValue();

					Calendar cal = Calendar.getInstance();
					cal.setTime(HSSFDateUtil.getJavaDate(d));
					cellText = (String.valueOf(cal.get(Calendar.YEAR))).substring(2);
					cellText = cal.get(Calendar.MONTH) + 1 + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/" + cellText;

					// ////System.out.println(cellText);

				}

				return cellText;
			} else if (cell.getCellType() == Cell.CELL_TYPE_BLANK)
				return "";
			else
				return String.valueOf(cell.getBooleanCellValue());
		} catch (Exception e) {

			e.printStackTrace();
			return "row " + rowNum + " or column " + colNum + " does not exist  in xls";
		}
	}

	// !isRowNull(i, rowNum)==true

	public int getTestCaseStartRowNo(String sheetName, int totalSheetRowCount) {

		int testCaseStartRownNum = 0;

		for (int testCaseStarNo = 1; testCaseStarNo <= totalSheetRowCount; testCaseStarNo++) {
			if ((getCellData(sheetName, 0, testCaseStarNo)).equalsIgnoreCase("TestName")) {

				testCaseStartRownNum = testCaseStarNo;
				break;
			}
		}

		return testCaseStartRownNum;

	}

	public int getTestCaseEndRowNo(String sheetName, int testCaseStartRownNum, int totalSheetRowCount) {

		int testCaseEndRownNum = 0;
		int testCaseRownNum = testCaseStartRownNum + 1;

		for (int rowEndNo = testCaseRownNum; rowEndNo <= totalSheetRowCount; rowEndNo++) {

			if ((getCellData(sheetName, 0, rowEndNo)).equalsIgnoreCase("TestName")) {

				testCaseEndRownNum = rowEndNo - 1;
				break;
			} else if (rowEndNo == totalSheetRowCount) {
				testCaseEndRownNum = rowEndNo;
				break;
			}

		}
		if (testCaseStartRownNum == testCaseEndRownNum) {
			testCaseEndRownNum = 0;
		}

		return testCaseEndRownNum;

	}

	public int getTestColCount(String sheetName, int testDataStartRowCount) {

		int lastColNum = 0;

		for (int i = 1; i <= 150; i++) {

			if ((getCellData(sheetName, i, testDataStartRowCount).isEmpty()
					|| getCellData(sheetName, i, testDataStartRowCount) == null)) {
				lastColNum = i;
				break;
			}
		}
		return lastColNum;

	}

	public Map<String, Object> samplePOC() throws IOException {

		int sheetCount = getSheetCount(workbook);
		try {

			for (int i = 0; i < sheetCount; i++) {
				String sheetName = getSheetName(i);
				int rows = getRowCount(sheetName);

				int testDataStartRowCount = 0;

				for (int testCaseStarNo = 1; testCaseStarNo <= rows; testCaseStarNo++) {

					if ((getCellData(sheetName, 0, testCaseStarNo)).equalsIgnoreCase("TestName")) {

						String testCaseName = getCellData(sheetName, 1, testCaseStarNo);

						// System.out.println("testCaseName========== > ::
						// "+testCaseName);

						testcaseMap.put(testCaseName,
								readAllData(sheetName, testCaseStarNo, rows, testDataStartRowCount));

					}
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("From Excel");

		return testcaseMap;

	}

	public Object[][] readAllData(String sheetName, int testCaseStarNo, int rows, int testDataStartRowCount) {

		Object[][] data = null;

		int testCaseEndRownNum = getTestCaseEndRowNo(sheetName, testCaseStarNo, rows);

		if (!(testCaseEndRownNum == 0)) {

			testDataStartRowCount = testCaseStarNo + 2;
			int colCount = getTestColCount(sheetName, testDataStartRowCount);

			if (testCaseEndRownNum == testDataStartRowCount) {
				data = new Object[1][colCount];
			} else {
				data = new Object[testCaseEndRownNum - testDataStartRowCount][colCount];
			}

			for (int rowNum = testDataStartRowCount + 1; rowNum <= testCaseEndRownNum; rowNum++) {

				for (int colNum = 0; colNum < colCount; colNum++) {

					if (!(row == null)) {
						// //System.out.println(""+ getCellData(sheetName,
						// colNum, rowNum));

						data[rowNum - (testDataStartRowCount + 1)][colNum] = ""
								+ getCellData(sheetName, colNum, rowNum);

					}

				}

			}

		}

		return data;

	}
	
	
	public String getCellData(int sheetIndex, int colNum, int rowNum) {
		try {
			if (rowNum <= 0)
				return "";

			//int index = workbook.getSheetIndex(sheetName);

			if (sheetIndex == -1)
				return "";

			sheet = workbook.getSheetAt(sheetIndex);
			row = sheet.getRow(rowNum - 1);
			if (row == null)
				return "";
			cell = row.getCell(colNum);
			if (cell == null)
				return "";

			if (cell.getCellType() == Cell.CELL_TYPE_STRING)
				return cell.getStringCellValue();

			else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
				String cellText = String.valueOf(cell.getNumericCellValue()).replace(".0", "");
				return cellText;
			} else if (cell.getCellType() == Cell.CELL_TYPE_FORMULA) {

				String cellText = String.valueOf(cell.getNumericCellValue());
				if (HSSFDateUtil.isCellDateFormatted(cell)) {
					// format in form of M/D/YY
					double d = cell.getNumericCellValue();

					Calendar cal = Calendar.getInstance();
					cal.setTime(HSSFDateUtil.getJavaDate(d));
					cellText = (String.valueOf(cal.get(Calendar.YEAR))).substring(2);
					cellText = cal.get(Calendar.MONTH) + 1 + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/" + cellText;

					// ////System.out.println(cellText);

				}

				return cellText;
			} else if (cell.getCellType() == Cell.CELL_TYPE_BLANK)
				return "";
			else
				return String.valueOf(cell.getBooleanCellValue());
		} catch (Exception e) {

			e.printStackTrace();
			return "row " + rowNum + " or column " + colNum + " does not exist  in xls";
		}
	}
	
	public int getTestCaseStartRowNo(int sheetIndex, int totalSheetRowCount) {

		int testCaseStartRownNum = 0;

		for (int testCaseStarNo = 1; testCaseStarNo <= totalSheetRowCount; testCaseStarNo++) {
			if ((getCellData(sheetIndex, 0, testCaseStarNo)).equalsIgnoreCase("TestCase Numbers")) {

				testCaseStartRownNum = testCaseStarNo;
				break;
			}
		}
		return testCaseStartRownNum;
	}

	
	public int getRowCount(int sheetIndex) {
		//int index = workbook.getSheetIndex(sheetName);
		if (sheetIndex == -1)
			return 0;
		else {
			sheet = workbook.getSheetAt(sheetIndex);
			int number = sheet.getLastRowNum() + 1;
			return number;
		}
	}
	

//	public int getColumnCountForFile(int sheetIndex) {
//		// check if sheet exists
////		if (!isSheetExist(sheetName))
////			return -1;
//
//		sheet = workbook.getSheetAt(sheetIndex);
//		//row = sheet.getRow(0);
//
////		if (row == null)
////			return -1;
//
//		return sheet.getLastCellNum();
//
//	}
//	
	
	
	public int getTestDataColumnIndex(int sheetIndex, String text) {
		int columnIndex = 0;

		int lastRow = getRowCount(0);
		for (int startRow = 1; startRow <= lastRow; startRow++) {
			Row eachRow = sheet.getRow(startRow-1);
			int rowLen = eachRow.getLastCellNum();
			for (int startCol = 0; startCol <= eachRow.getLastCellNum(); startCol++)
			{
				String abc =  getCellData(sheetIndex, startCol, startRow);
//				System.out.println("Valueeeeeeeee " + abc);
				if ((getCellData(sheetIndex, startCol, startRow).equalsIgnoreCase(text))) {
					columnIndex = startCol;
					break;
				}
			}
		}
		return columnIndex;
	}
	
	public int getTestCaseStartRowNumber(int sheetIndex, int totalSheetRowCount, String textMatch) {
		
		int rowIndex = 0;
		boolean targetFound = false;

		//int lastRow = getRowCount(0);
		for (int startRow = 1; startRow <= totalSheetRowCount; startRow++) {
			Row eachRow = sheet.getRow(startRow-1);
			for (int startCol = 0; startCol < eachRow.getLastCellNum(); startCol++)
			{
				String abc = getCellData(sheetIndex, startCol, startRow);
//				System.out.println("Valueeeee "+ abc);
				if ((getCellData(sheetIndex, startCol, startRow).equalsIgnoreCase(textMatch))) {
					rowIndex = startRow;
					targetFound = true;
					break;
				}
			}
			if(targetFound) {
				break;
			}
		}
		return rowIndex;
	}
	
	public String GetResponseValueForTestID(String testId, String xmlResponseObject) {
		
		int getRowIndex = getTestCaseStartRowNumber(0, getRowCount(0), testId);
		int getColumnIndex = getTestDataColumnIndex(0, xmlResponseObject);
		
//		System.out.println("Got the Row index and Column Index, Now get the value for corresponding index");
		return getCellData(0, getColumnIndex, getRowIndex);	
		//return toString();
	}
	

}
